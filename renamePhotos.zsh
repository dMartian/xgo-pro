#!/bin/zsh
zmodload zsh/mathfunc
set -ueo extendedglob

EXIFTOOLPATH="/opt/local/bin"
PARALLELISM=128
FORMATS=("AVI" "CR2" "DNG" "JPEG" "JPG" "MOV" "MP4" "PEF" "PNG" "THM" "XMP")

PATH=${EXIFTOOLPATH}:${PATH}
function logg {
    echo $(date "+%H:%M:%S")" $1"
}

function delete_files {
    DELENDA=($*)
    for d in ${DELENDA}; do rm ${d}; done
}

function get_new_name {
    ## gets new file name without extension
    local file=$1
    local fileBaseName=${file:t:r}
    local dirName=${file:h}
    local timeStamp=$(exiftool -m -d "%Y%m%d-%H%M%S" -p '$DateTimeOriginal' "${file}")
    local camera
    local newname
    if [[ -z "${timeStamp}" ]]; then
        timeStamp=$(exiftool -m -d "%Y%m%d-%H%M%S" -p '$CreateDate' "${file}")
    fi
    if [[ -z "${timeStamp}" ]]; then
        logg "get_new_name ${file}: no timeStamp found -> not renaming" >> renamePhotos.err
        newname=${fileBaseName} ### do not rename
    else
        timeStamp=$(echo ${timeStamp}|sed 's/0000:00:00 00:00:00/noTimeStamp/')
        camera=$(exiftool -m -p '$UniqueCameraModel' -S ${file})
        if [[ -z "${camera}" ]]; then
            camera=$(exiftool -m -p '$Make $Model' -S ${file})
        fi
        if [[ -z "${camera}" ]]; then
            camera="noCamera"
        fi
        camera=$(echo ${camera}|sed 's/ /_/g;s/_*(.*//;s/Leica_Camera_AG_//')
        case ${camera} in
            FUJIFILM_FinePix_JZ250/JZ260 )
                camera="FUJI_FinePix_JZ250" ;;
            LEICA_M_MONOCHROM )
                camera="LEICA_M_MONO" ;;
            PENTAX_PENTAX_K-5 )
                camera="PENTAX_K5" ;;
        esac
        newname=${timeStamp//\//_}"_"${camera//\//_}
        if [[ "${newname}" == "-            " ]]; then
            newname=${fileBaseName}
        else
            newname=${newname//_(#c2,)/_}
        fi
    fi
    if [[ "${newname}" == "${fileBaseName}" ]]; then
        logg "get_new_name ${file}: could not get newname. Please check. Returning ${newname}" >> renamePhotos.err
    fi
    echo "${newname}"
}

function rename_file {
    local file=$1
    #logg "rename_file: file = ${file}"
    local dirName=${file:h}
    local fileNameWithoutExt=${file:t:r}
    local extension=${file:e:u} # extension in upper case because some filesystems are case insensitive
    local finalname=${file//*\.\.\./} ## without path
    local newDirFilename="${dirName}/${finalname}"
    #logg "rename_file: newDirFilename = ${newDirFilename}"
    local existing_targets=($(find ${dirName} -maxdepth 1 -name "${finalname:t:r}*.${extension}"))
    #logg "rename_file: NUM_existing_targets = $#existing_targets"
    if (($#existing_targets == 1)); then
        #logg "rename_file: existing_targets: ${existing_targets}"
        #logg "rename_file: mv -n ${newDirFilename} ${newDirFilename:r}-000.${extension}"
        mv -n ${newDirFilename} ${newDirFilename:r}-000.${extension}
    fi

    if (($#existing_targets > 0 )); then
        newDirFilename="${newDirFilename:r}"-$(printf "%03d\n" $#existing_targets).${extension}
    fi

    #logg "rename_file: mv -n ${file} ${newDirFilename}"
    mv -n ${file} ${newDirFilename}
    chmod 0644 ${newDirFilename}
}

function move_file_to_tmp_file {
    local ndigits=$1
    local file=$2
    local index=$3

    local dirName=${file:h}
    local fileName=${file:t}
    local extension=${file:e:u} # extension in upper case because some filesystems are case insensitive

    local numberFormat="_%0$((ndigits))d_"
    local formattedIndex=$(printf ${numberFormat} $((index)))
    local newname=$(get_new_name ${file})
    #logg "move_file_to_tmp_file: file = ${file}  ,  newname = ${newname}"
    local tmpName=${dirName}/${formattedIndex}${fileName}...${newname}.${extension}

    mv -n ${file} ${tmpName}
}

function rename_all_files_of_type {
    local folderName=$1
    local fileExt=$2
    echo ""
    logg "START rename_all_files_of_type ${folderName} ${fileExt}"
    local bilder=($(find ${folderName} -type f -iname "*.${fileExt}" | sort))
    local num_bilder=$#bilder
    local numDigits=3
    local zaehler=1
    logg "rename_all_files_of_type: num_bilder = $((num_bilder))"
    if (( 0 < num_bilder )) ; then
        numDigits=$((1+int(log(num_bilder)/log(10))))
        logg "rename_all_files_of_type: numDigits=$((numDigits))"
        logg "rename_all_files_of_type: start loop calling move_file_to_tmp_file: $num_bilder files"
        for i in ${bilder}; do
            move_file_to_tmp_file $((numDigits)) $i $((zaehler)) &
            if (( 0 == zaehler%PARALLELISM )); then
                logg "rename_all_files_of_type, loop move_file_to_tmp_file: zaehler=$((zaehler)) => waiting"
                wait
            fi
            zaehler=$((1+zaehler))
        done
        wait

        logg "rename_all_files_of_type: start loop calling rename_file: $num_bilder files"
        bilder=($(find ${folderName} -type f -iname "*.${fileExt}"))
        for i in ${bilder}; do
            rename_file $i
        done
        logg "DONE rename_all_files_of_type ${folderName} ${fileExt}"
    else logg "rename_all_files_of_type: Nothing to do :)";
    fi
}

function rename_folder_content {
    local folderName=$1
    logg "START rename_folder_content ${folderName}"
    logg "FORMATS = ${FORMATS}"
    for FORMAT in ${FORMATS}; do
        rename_all_files_of_type ${folderName} ${FORMAT}
    done
    logg "DONE rename_folder_content ${folderName}"
}

function work_on_folder {
    local directory=$1
    local errFile="${directory}/renamePhotos.err"
    local outFile="${directory}/renamePhotos.out"
    exec 1>${outFile} 2>${errFile}
    logg "START work_on_directory ${directory}"
    delete_files $(find ${directory} -type f -name "*_original")
    delete_files $(find ${directory} -type f -name "*tmp")
    rename_folder_content ${directory}
    if [[ -a $errFile && ! -s $errFile ]]; then
        rm $errFile
    fi
    delete_files $(find ${directory} -type f -name "*_original")
    delete_files $(find ${directory} -type f -name "*tmp")
    logg "DONE work_on_directory ${directory}"
}


for folder in $@
do
    work_on_folder ${folder}
done

logg "DONE $0 $*"

exit 0

# eof
